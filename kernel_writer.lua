require 'torch'
require 'nn'
require 'image'

net = torch.load('./results/lcn500.net')

local conv1 = net:get(1):get(1)
local ker = torch.cat(conv1.bias, conv1.weight, 2)

image.display({image=ker})
