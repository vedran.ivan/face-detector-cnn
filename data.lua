
require 'torch'   -- torch
require 'nn'
require 'optim'
require 'image'   -- to visualize the dataset
require 'nnx'      -- provides a normalization operator
require 'xlua'

local opt = {}
opt.data_size = 7000
opt.train_size = (9/10)*opt.data_size
opt.test_size = opt.data_size - opt.train_size
opt.visualize = true
opt.path = '/home/vedran/wider_32/'
opt.ext = 'png'

--parametri za optim.sgd(, , optimState)
optimState = {
	nesterov = true,
	learningRate = 0.0001,
	learningRateDecay = 1e-7,
	momentum = 0.9,
	dampening = 0,
}

--change this to point to image dir with faces/bg folders
local dir_path='/home/vedran/wider_32/'
--------------------------- LOAD DATASET --------------------------------------
-------------------------------------------------------------------------------
local imagesAll = torch.Tensor(2*opt.data_size,3,32,32)
local labelsAll = torch.Tensor(2*opt.data_size)

classes = {'face','backg'}
local index = 1

local function loadFaces()
	print(sys.COLORS.green..'==> loading faces!')
  for file in paths.files(paths.concat(opt.path,'faces')) do
		fqn = paths.concat(opt.path,'faces',file)
		if paths.extname(fqn) == opt.ext then
      if index > opt.data_size then break end
      imagesAll[index] = image.load(fqn):float()
      labelsAll[index] = 1
      index = index + 1
      xlua.progress(index,opt.data_size)
    end
	end
end

local function loadBackgrounds()
	print(sys.COLORS.green..'==> loading backgrounds!')
	for file in paths.files(paths.concat(opt.path,'bg')) do
		fqn = paths.concat(opt.path,'bg',file)
		if paths.extname(fqn) == opt.ext then
      if index > 2* opt.data_size then break end
      imagesAll[index] = image.load(fqn):float()
      labelsAll[index] = 2
      index = index + 1
      xlua.progress(index-opt.data_size,opt.data_size)
    end
	end
end


local labelsShuffle = torch.randperm((#labelsAll)[1])

-- create train set:
trainData = {
   data = torch.Tensor(opt.train_size, 1, 32, 32),
   labels = torch.Tensor(opt.train_size),
   size = function() return opt.train_size end
}
--create test set:
testData = {
      data = torch.Tensor(opt.test_size, 1, 32, 32),
      labels = torch.Tensor(opt.test_size),
      size = function() return opt.test_size end
   }

loadFaces()
loadBackgrounds()
if opt.visualize then
	 local samples = imagesAll[{ {1,256}, {} }]
	 image.display{image=samples, nrow=16, legend='WIDER Faces',padding=1}
	 image.save('wider_faces',samples)
end
--------------------------------PREPROCESS ----------------------------------
-----------------------------------------------------------------------------


local function preprocessing()
			 print '==> preprocessing data: colorspace RGB -> YUV'
			 for i = 1,imagesAll:size(1) do
			 	   imagesAll[i] = image.rgb2yuv(imagesAll[i])
			 end

		local channels = {'y'}
		--------------------------------------------------------------------------------
		--GLOBAL NORMALIZATION
		--[[
		local channels = {'y'}
		print(sys.COLORS.red ..  '==> preprocessing data: normalize each feature (channel) globally')
		local mean = {}
		local std = {}
		for i,channel in ipairs(channels) do
		   -- normalize each channel globally:
		   mean[i] = imagesAll[{ {},i,{},{} }]:mean()
		   std[i] =  imagesAll[{ {},i,{},{} }]:std()
		   imagesAll[{ {},i,{},{} }]:add(-mean[i])
		   imagesAll[{ {},i,{},{} }]:div(std[i])
		end]]

		--LOCAL NORMALIZATION
		print(sys.COLORS.red ..  '==> preprocessing data: LCN ')

		local neighborhood = image.gaussian1D(5) -- 5 for face detector training

		local normalization = nn.SpatialContrastiveNormalization(1, neighborhood, 1):float()

		-- Normalize all channels locally:
		for c in ipairs(channels) do
		   for i = 1,imagesAll:size(1) do
		      imagesAll[{ i,{c},{},{} }] = normalization:forward(imagesAll[{ i,{c},{},{} }]:float())
		   end
		end

	--------------------------------------------------------------------------------
	if opt.visualize then
		 local samples = imagesAll[{ {1,256}, {} }]
		 image.display{image=samples, nrow=16, legend='imagesAll processed',padding=1}
	end
end

		--preprocessing()
   if not (paths.filep('train_data.t7') and paths.filep('test_data.t7')) then
     loadFaces()
     loadBackgrounds()
		 preprocessing()
		 --preproess
		 print(sys.COLORS.green..'==> preparing train data!')
     for i=1,opt.train_size do
        trainData.data[i] = imagesAll[labelsShuffle[i]][1]:clone()
        trainData.labels[i] = labelsAll[labelsShuffle[i]]
				xlua.progress(i, opt.train_size)
     end
		 print(sys.COLORS.green..'==> preparing test data!')
     for i=opt.train_size+1, opt.data_size do
        testData.data[i-opt.train_size] = imagesAll[labelsShuffle[i]][1]:clone()
        testData.labels[i-opt.train_size] = labelsAll[labelsShuffle[i]]
				xlua.progress(i-opt.train_size, opt.data_size)
     end

     torch.save('train_data.t7', trainData)
     torch.save('test_data.t7', testData)

   else
     trainData = torch.load('train_data.t7')
     testData = torch.load('test_data.t7')
  end


   imagesAll = nil
   labelsAll = nil

--------------------------------------------------------------------------------
trainData.data = trainData.data:float()
testData.data = testData.data:float()
--------------------------------------------------------------------------------
local channels = {'y'}

print(sys.COLORS.red ..  '==> verify statistics')
		for i,channel in ipairs(channels) do
		 local trainMean = trainData.data[{ {},i }]:mean()
		 local trainStd = trainData.data[{ {},i }]:std()

		 local testMean = testData.data[{ {},i }]:mean()
		 local testStd = testData.data[{ {},i }]:std()

		 print('training data, '..channel..'-channel, mean: ' .. trainMean)
		 print('training data, '..channel..'-channel, standard deviation: ' .. trainStd)

		 print('test data, '..channel..'-channel, mean: ' .. testMean)
		 print('test data, '..channel..'-channel, standard deviation: ' .. testStd)
		end

	 --visualize some data
	 if opt.visualize then
	    local samples = trainData.data[{ {1,256}, {} }]
	    image.display{image=samples, nrow=16, legend='faces.non-faces after 1D flatening',padding=1}
	 end




--[[
--------------------------------PREPROCESS ----------------------------------
-----------------------------------------------------------------------------

		 print '==> preprocessing data: colorspace RGB -> YUV'
		 for i = 1,#trainData do
		 	   trainData.data[i] = image.rgb2yuv(trainData.data[i])
		 end

		 for i = 1,#testData do
		 		testData.data[i] = image.rgb2yuv(testData.data[i])
		 end
	--------------------------------------------------------------------------------
	local channels = {'y','u','v'}

	print(sys.COLORS.red ..  '==> preprocessing data: normalize each feature (channel) globally')
	local mean = {}
	local std = {}
	for i,channel in ipairs(channels) do
	   -- normalize each channel globally:
	   mean[i] = trainData.data[{ {},i,{},{} }]:mean()
	   std[i] = trainData.data[{ {},i,{},{} }]:std()
	   trainData.data[{ {},i,{},{} }]:add(-mean[i])
	   trainData.data[{ {},i,{},{} }]:div(std[i])
	end

	-- Normalize test data, using the training means/stds
	for i,channel in ipairs(channels) do
	   -- normalize each channel globally:
	   testData.data[{ {},i,{},{} }]:add(-mean[i])
	   testData.data[{ {},i,{},{} }]:div(std[i])
	end

	-- Local contrast normalization is needed in the face dataset as the dataset is already in this form:
	print(sys.COLORS.red ..  '==> preprocessing data: normalize all three channels locally')

	-- Define the normalization neighborhood:
	local neighborhood = image.gaussian1D(5) -- 5 for face detector training

	-- Define our local normalization operator (It is an actual nn module,
	-- which could be inserted into a trainable model):
	local normalization = nn.SpatialContrastiveNormalization(1, neighborhood, 1):float()

	-- Normalize all channels locally:
	for c in ipairs(channels) do
	   for i = 1,trainData:size() do
	      trainData.data[{ i,{c},{},{} }] = normalization:forward(trainData.data[{ i,{c},{},{} }])
	   end
	   for i = 1,testData:size() do
	      testData.data[{ i,{c},{},{} }] = normalization:forward(testData.data[{ i,{c},{},{} }])
	   end
	end

	----------------------------------------------------------------------
	print(sys.COLORS.red ..  '==> verify statistics')

	-- It's always good practice to verify that data is properly
	-- normalized.

	for i,channel in ipairs(channels) do
	   local trainMean = trainData.data[{ {},i }]:mean()
	   local trainStd = trainData.data[{ {},i }]:std()

	   local testMean = testData.data[{ {},i }]:mean()
	   local testStd = testData.data[{ {},i }]:std()

	   print('training data, '..channel..'-channel, mean: ' .. trainMean)
	   print('training data, '..channel..'-channel, standard deviation: ' .. trainStd)

	   print('test data, '..channel..'-channel, mean: ' .. testMean)
	   print('test data, '..channel..'-channel, standard deviation: ' .. testStd)
	end

--------------------------------------------------------------------------------
if opt.visualize then
	 local samples = trainData.data[{ {1,256}, {} }]
	 image.display{image=samples, nrow=16, legend='data after preprocess'}
end
]]
--print( trainData.data[1] )

return {
  trainData = trainData,
  testData  = testData,
	mean = mean,
	std = std,
	classes = classes
}
