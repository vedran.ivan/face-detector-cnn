
require 'torch'   -- torch
require 'image'   -- to visualize the dataset
require 'nn'      -- provides all sorts of trainable modules/layers
--require 'Dropout' -- Hinton dropout technique



if opt.type == 'cuda' then
   nn.SpatialConvolutionMM = nn.SpatialConvolution
end

----------------------------------------------------------------------
print(sys.COLORS.red ..  '==> define parameters')

-- 2-class problem: faces!
local noutputs = 2

-- input dimensions: faces!
local nfeats = 1
local width = 32
local height = 32

-- hidden units, filter sizes (for ConvNet only):
local nstates = {16,32}
local filtsize = {5, 7}
local poolsize = 4

----------------------------------------------------------------------
print(sys.COLORS.red ..  '==> construct CNN')

local CNN = nn.Sequential()

-- stage 1: conv+max
CNN:add(nn.SpatialConvolutionMM(nfeats, nstates[1], filtsize[1], filtsize[1]))
-- izlaz 16x28x28
CNN:add(nn.SpatialMaxPooling(poolsize,poolsize,poolsize,poolsize))
CNN:add(nn.ReLU())
--izlaz 16x7x7
-- stage 2: conv+max
CNN:add(nn.SpatialConvolutionMM(16, 16, 7, 7))
CNN:add(nn.ReLU())

CNN:add(nn.SpatialConvolutionMM(16,2,1,1))
--izlaz 16x2x1

CNN:add(nn.SpatialSoftMax())
CNN:add(nn.AddConstant(0.000000001))
CNN:add(nn.Log())


for _,layer in ipairs(CNN.modules) do
   if layer.bias then
      layer.bias:fill(.2)
      if i == #CNN.modules-1 then
         layer.bias:zero()
      end
   end
end

local model = nn.Sequential()
model:add(CNN)

-- Loss: NLL
loss = nn.CrossEntropyCriterion()


----------------------------------------------------------------------
print(sys.COLORS.red ..  '==> here is the CNN:')
print(model)

if opt.type == 'cuda' then
   model:cuda()
   loss:cuda()
end

-- return package:
return {
   model = model,
   loss = loss,
}
