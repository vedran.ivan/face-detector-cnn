from PIL import Image, ImageFilter
import random as rand
import numpy as np
import os.path
import h5py

"""
labels:
expression_label_list
face_bbx_list
file_list
illumination_label_list
blur_label_list
event_list
invalid_label_list
occlusion_label_list
pose_label_list
"""

f = h5py.File('wider_face_train.mat','r')
files = f['file_list'][()]
faces = f['face_bbx_list'][()]

def getFileName(obj):
    str1 = ''.join(chr(i) for i in obj[()])
    return str1

index=1

for z in range (0, 61):
    #imena
    hdf_files_folder = f[files[0,z]][()]
    #bbx pozicije
    hdf_img_folder = f[faces[0,z]][()]

    for i in range(0, hdf_img_folder.shape[1]):

        hdf_file = f[hdf_files_folder[0,i]]
        file = getFileName(hdf_file)
        im_path = '../WIDER_ALL/'+file+'.jpg'
        if os.path.exists(im_path):
            im = Image.open(im_path)
        else:
            print "nope"
            continue

        #for background generate random position
        step = 10
        out_size = 32
        for k in range(0,step):
            rand.seed()
            pos = rand.randint(0,im.size[0]-100), rand.randint(0,im.size[1]-100)
            bg = (pos[0], pos[1], pos[0]+out_size, pos[1]+out_size)
            bg_crop = im.crop(bg)
            bg_crop.save('../wider_32/bg/bg'+str(i*step+k+1)+'.png')

        if index%500==0:
            print index, "faces"
            print i*10, "backgrounds"

        hdf_img = hdf_img_folder[0,i]
        boxes = f[hdf_img][()]
        for j in range(0, boxes.shape[1]):
            bbx = boxes[:,j]
            x, y, w, h = bbx[0], bbx[1], bbx[2], bbx[3]
            if w >= 100:
                bbx2 = (x, y, w+x, h+y)
                bbx_crop = im.crop(bbx2)
                bbx_normalized = bbx_crop.resize((out_size,out_size))
                bbx_normalized.save('../wider_32/faces/face'+str(index)+'.png')
                index+=1

    #bbx_normalized.show()


#im.show()

#new_img = im.resize((int(round(im.size[0]*0.2)), int(round(im.size[1]*0.2))))

#print (im.format, im.size, im.mode)
