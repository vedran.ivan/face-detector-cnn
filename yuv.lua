require 'image'
require 'torch'
require 'nn'

img = image.load('face-02.jpg')


t = torch.Tensor(3,500,500)
t = image.scale(t,img)
yuv_img = image.rgb2yuv(t)
y = yuv_img[{{1,1},{},{}}]
u = yuv_img[{{2,2},{},{}}]
v = yuv_img[{{3,3},{},{}}]

t2 = torch.Tensor(1,500,1000)
t3 = torch.Tensor(1,500,1500)

torch.cat(t2,y,v,3)
torch.cat(t3,t2,v,3)

image.display{image=t2, padding=5}
image.save('yuv.png',t2)
