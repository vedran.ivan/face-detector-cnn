require 'torch'
require 'nn'
require 'image'
require 'PyramidPacker'
require 'PyramidUnPacker'


local test_data = torch.load('test_data.t7')
print("test data size: ", #test_data.data)
local net = torch.load('./results/lcn500.net'):double()


--face!
face = test_data.data[1]:double() -- 1 for face
--image.display({image= face})
--bg!
bg = test_data.data[3]:double() -- 2 for bg
--image.display({image= bg})

	 --visualize some data
if true then
  local samples = test_data.data[{ {1,256}, {} }]
	--image.display{image=samples, nrow=16, legend='data before preprocess'}
end

local function runClassCrit(frame)
  local crit = nn.ClassNLLCriterion()
  local pred = net:forward(frame)
  local res_face = crit:forward(pred, 1)
  local res_bg = crit:forward(pred,2)
  if res_face < res_bg then
    diff = math.abs(res_bg-res_face)
    --if (math.abs(res_bg-res_face)/res_face)>1000. or (math.abs(res_face)<1e-6 and math.abs(res_bg)>3)
        --or (math.abs(res_face)<0.5 and math.abs(res_bg)>8 )then
      if (math.abs(res_face)<1e-9 and math.abs(res_bg)>12)then
        --print("face: ", res_face, "bg: ", res_bg)
        return 1
      end
  end
  return 2
end


print("face",runClassCrit(face))

print("bg",runClassCrit(bg))
--------------------------------------------------------------------------------
-----------------------------------PROCESS IMAGE-------------------------------
local channels = {'y','u','v'}
local mean = {}
local std = {}

img_orig = image.load('people.jpg'):float()
imgc = img_orig:clone():float()
img = image.rgb2yuv(img_orig):clone()

--tensor for keeping track of detection windows
local detection_map = torch.Tensor(1,img_orig:size(2),img_orig:size(3)):fill(0)

for i,channel in ipairs(channels) do
   mean[i] = img[{i,{},{} }]:mean()
   std[i] = img[{i,{},{} }]:std()
   img[{i,{},{} }]:add(-mean[i])
   img[{i,{},{} }]:div(std[i])
end

local neighborhood = image.gaussian1D(5)
local normalization = nn.SpatialContrastiveNormalization(1, neighborhood, 1):float()

for c in ipairs(channels) do
  img[{{c},{},{}}] = normalization:forward(img[{{c},{},{}}])
end


--image.display{image=img[{1,{},{}}], legend='test image1' }
--image.display{image=img[{2,{},{}}], legend='test image2' }
--image.display{image=img[{3,{},{}}], legend='test image3' }

local positive_frames = {}

local negative_frames = {}

local Pindex = 1
local  Nindex = 1

w = img:size(3)
h = img:size(2)
scales = { 0.075, 0.08, 0.085, 0.09, 0.095}

img2 = img_orig:clone()

--Flaten image to 1D
img = img[{{1,1},{},{}}]

image.display({image=img, legend='width: '..img:size(3)..'height: '..img:size(2)})
--img_orig = image.drawRect(img_orig,1,1,math.min(w,h) * scales[1],math.min(w,h) *scales[1], {color={255,0,0}})

for i,s in ipairs(scales) do
  buff_x = math.min(w,h) * s
  buff_y = buff_x * 1.2
  step = math.min(w,h) * s * 0.1
  print("scale..",s)
  for y=1,h,step do

    for x=1,w,step do
      if(x+buff_x > w) or (y+buff_y > h) then break end
      --print("step, buff_size: ",step, buff)
      cr = img[{{},{y,y+buff_y},{x,x+buff_x}}]:clone():float()
      rescr = image.scale(cr,32,32):double()
      detection = runClassCrit(rescr)
      --image.drawRect(img2,x,y,x+buff_x,y+buff_y, {linewidth=2,inplace=true, color = {0,255,0}})

      if detection==1 then
        image.drawRect(img_orig,x,y,x+buff_x,y+buff_y, {linewidth=2,inplace=true, color = {0,255,0}})
        positive_frames[Pindex] = rescr
        detection_map[{{},{y,y+buff_y},{x,x+buff_x}}]:add(1)
        Pindex = Pindex + 1
      else
        negative_frames[Nindex] = rescr
        Nindex = Nindex + 1
      end

    end
  end
end

--print(detection_map)
--image.display({image=detection_map})
------------------------------------------------------------------------------


--image.display({image=img2, legend='width: '..img:size(3)..'height: '..img:size(2)})
--image.display({image=positive_frames, nrow=8, legend='positive detections'})
--image.display({image=negative_frames, nrow=8, legend='positive detections'})
image.display{image=img_orig}
image.save('detections3.jpg',img_orig)

print(sys.COLORS.red..'Number of positive detections: '..Pindex)
print(sys.COLORS.red..'Number of negative detections: '..Nindex)
print(sys.COLORS.red..'Ratio: positive/negative= : '..Pindex/Nindex)
