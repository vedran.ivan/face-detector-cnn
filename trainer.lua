require 'torch'   -- torch
require 'nn'
require 'optim'
require 'image'   -- to visualize the dataset
require 'nnx'      -- provides a normalization operator
