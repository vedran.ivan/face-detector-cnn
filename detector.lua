-------------------------------------------------------------------------------
-- CNN face detector
-- Vedran Ivanusic
-------------------------------------------------------------------------------
require 'torch'
require 'nn'
require 'image'
require 'PyramidPacker'
require 'PyramidUnPacker'


local test_data = torch.load('test_data.t7')
print("test data size: ", #test_data.data)
local net = torch.load('./results/model.net'):double()


--face!
face = test_data.data[1]:double() -- 1 for face
--image.display({image= face})
--bg!
bg = test_data.data[3]:double() -- 2 for bg
--image.display({image= bg})

	 --visualize some data
if true then
  local samples = test_data.data[{ {1,256}, {} }]
	--image.display{image=samples, nrow=16, legend='data before preprocess'}
end

local function runClassCrit(frame)
  local crit = nn.CrossEntropyCriterion()
  local pred = net:forward(frame)
  local res_face = crit:forward(pred, 1)
  local res_bg = crit:forward(pred,2)

  if res_face < res_bg then
    diff = math.abs(res_bg-res_face)
    --if (math.abs(res_bg-res_face)/res_face)>1000. or (math.abs(res_face)<1e-6 and math.abs(res_bg)>3)
        --or (math.abs(res_face)<0.5 and math.abs(res_bg)>8 )then
      if ( res_face<1e-4 and res_bg>10 and res_bg<18)then
        --print("face: ", res_face, "bg: ", res_bg)
        return 1
      end
  end
  return 2
end


--------------------------------------------------------------------------------
-----------------------------------PROCESS IMAGE-------------------------------
local channels = {'y','u','v'}
local mean = {}
local std = {}

img_orig = image.load('people.jpg'):float()
imgc = img_orig:clone():float()
img = image.rgb2yuv(img_orig):clone()

--tensor for keeping track of detection windows
local detection_map = torch.Tensor(1,img_orig:size(2),img_orig:size(3)):fill(0)

for i,channel in ipairs(channels) do
   mean[i] = img[{i,{},{} }]:mean()
   std[i] = img[{i,{},{} }]:std()
   img[{i,{},{} }]:add(-mean[i])
   img[{i,{},{} }]:div(std[i])
end

local neighborhood = image.gaussian1D(5)
local normalization = nn.SpatialContrastiveNormalization(1, neighborhood, 1):float()

for c in ipairs(channels) do
  img[{{c},{},{}}] = normalization:forward(img[{{c},{},{}}])
end

-------------------------------------------------------------------------------
local positive_frames = {}
local negative_frames = {}
local Pindex = 1
local  Nindex = 1
w = img:size(3)
h = img:size(2)
scales = { 0.075, 0.08, 0.085, 0.09, 0.095}

--Flaten image to 1D
img = img[{{1,1},{},{}}]


----------------------------------DETECTION VARIABLES---------------------------
windows = {}
detections = {}
windex = 1 --index in windows
local last_window = torch.Tensor(4):fill(-10)
local current_window = torch.Tensor(4):fill(0)
local diff = 20 --difference for detecting overlapping face detection windows
local wnd = torch.Tensor(4)
--------------------------------------------------------------------------------
local function addToTable(tb, window, new_row )
    wnd:copy(window)
    if new_row==true and table.getn(tb)~=0 then
      windex = windex +1
    end
    windex = windex or 1
    row = tb[windex] or {}
    ax = row[1] or window[1]
    ay = row[2] or window[2]
    aw = row[3] or window[3]
    ah = row[4] or window[4]
    row = {(window[1]+ax)/2, (window[2]+ay)/2, (window[3]+aw)/2, (window[4]+ah)/2}
    tb[windex] = row
end

local function squareDiff(x1, y1, x2, y2)
  a = x1-x2
  b = y1-y2
  --print(a,b)
  return math.sqrt(math.pow(a,2)+math.pow(b,2))
end

for i,s in ipairs(scales) do
  buff_x = math.min(w,h) * s
  buff_y = buff_x * 1.2
  step = math.min(w,h) * s * 0.1
  print("scale..",s)
  for y=1,h,step do

    for x=1,w,step do
      if(x+buff_x > w) or (y+buff_y > h) then break end

      cr = img[{{},{y,y+buff_y},{x,x+buff_x}}]:clone():float()
      rescr = image.scale(cr,32,32):double()
      detection = runClassCrit(rescr)
      --image.drawRect(img2,x,y,x+buff_x,y+buff_y, {linewidth=2,inplace=true, color = {0,255,0}})

      if detection==1 then
        image.drawRect(img_orig,x,y,x+buff_x,y+buff_y, {linewidth=2,inplace=true, color = {0,255,0}})
        positive_frames[Pindex] = rescr
        detection_map[{{},{y,y+buff_y},{x,x+buff_x}}]:add(1)
        last_window = last_window:copy(current_window)
        current_window[1] = x
        current_window[2] = y
        current_window[3] = x+buff_x
        current_window[4] = y+buff_y

        if squareDiff(x,y,last_window[1],last_window[2]) < diff then
          --same face
          addToTable(detections,current_window, false)
        else
          --another face
          addToTable(detections,current_window, true)
        end

        Pindex = Pindex + 1
      else
        negative_frames[Nindex] = rescr
        Nindex = Nindex + 1
      end

    end
  end
end


local function printTable(tbl)
  for i=1,table.getn(tbl) do
    entry = tbl[i]
    print("x: ",entry[1],"y: ",entry[2],"dx: ",entry[3],"dy: ",entry[4])
  end
end

print("size: ",table.getn(windows) )

pruned = {}
pindex = 1


for i,detect in ipairs(detections) do
  local duplicate = 0

  for j,prune in ipairs(pruned) do
    if squareDiff(detect[1],detect[2],prune[1],prune[2]) < 50 then
      duplicate = 1
      pruned[j] = {(prune[1]+detect[1])/2,(prune[2]+detect[2])/2,(prune[3]+detect[3])/2,(prune[4]+detect[4])/2}
      detections[i] = {0,0,0,0}
    end
  end

  if duplicate == 0 then
      pruned[pindex] = {detect[1],detect[2],detect[3],detect[4]}
      pindex = pindex + 1
  end
end

print("pruned size: ", table.getn(pruned))
printTable(pruned)

for i,window in ipairs(pruned) do
  image.drawRect(imgc,window[1],window[2],window[3],window[4], {linewidth=6,inplace=true, color = {0,0,255}})
end
image.save('results.jpg',imgc)

--local filter = nn.Sequential()
--filter:add(nn.SpatialMaxPooling(10,10,1,1))

--filtered_img = filter:forward(detection_map)

--image.display{image=filtered_img}
image.display{image=imgc}
image.display{image=img_orig}
image.save('cross-entropy.jpg',img_orig)

--[[
print(sys.COLORS.red..'Number of positive detections: '..Pindex)
print(sys.COLORS.red..'Number of negative detections: '..Nindex)
print(sys.COLORS.red..'Ratio: positive/negative= : '..Pindex/Nindex)

]]

return {
  pruned = pruned
}
