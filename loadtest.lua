
require 'torch'   -- torch
require 'nn'
require 'optim'
require 'image'   -- to visualize the dataset
require 'nnx'      -- provides a normalization operator
require 'xlua'

local opt = {}
opt.data_size = 7000
opt.train_size = (9/10)*opt.data_size
opt.test_size = opt.data_size - opt.train_size
opt.visualize = true
opt.path = '/home/vedran/wider_32/'
opt.ext = 'png'
--------------------------- LOAD DATASET --------------------------------------
-------------------------------------------------------------------------------
local imagesAll = torch.Tensor(2*opt.data_size,3,32,32)
local labelsAll = torch.Tensor(2*opt.data_size)

classes = {'face','backg'}
local index = 1

local function loadFaces()
  print(sys.COLORS.green..'==> loading faces!')
  for file in paths.files(paths.concat(opt.path,'faces')) do
		fqn = paths.concat(opt.path,'faces',file)
		if paths.extname(fqn) == opt.ext then
      if index > opt.data_size then break end
      imagesAll[index] = image.load(fqn):float()
      labelsAll[index] = 1
      index = index + 1
      xlua.progress(index,opt.data_size)
    end
	end
end

local function loadBackgrounds()
  print(sys.COLORS.green..'==> loading backgrounds!')
	for file in paths.files(paths.concat(opt.path,'bg')) do
		fqn = paths.concat(opt.path,'bg',file)
		if paths.extname(fqn) == opt.ext then
      if index > 2* opt.data_size then break end
      imagesAll[index] = image.load(fqn):float()
      labelsAll[index] = 2
      index = index + 1
      xlua.progress(index-opt.data_size,opt.data_size)
    end
	end
end

loadFaces()
loadBackgrounds()

local samples = imagesAll[{ {1,256}, {} }]
image.display{image=samples, nrow=16, legend='imagesAll processed',padding=1}
